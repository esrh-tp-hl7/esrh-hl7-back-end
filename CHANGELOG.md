# ESRH HL7 BACK-END CHANGELOG

## Version 1.1.0 (24-01-2019)

* [FEA] Add patient's history
* [FEA] Add check if the new action date is after the previous action date

## Version 1.0.0 (20-01-2019)

* First version of back-end
