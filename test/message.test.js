/* global describe, it */
/**
 * @file message.test.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   Test file for the message model.
 */
const chai = require('chai');
// eslint-disable-next-line no-unused-vars
const should = chai.should();
const hl7 = require('L7');
const fs = require('fs');
const Message = require('../models/message');

describe('Message module', () => {
  describe('#constructor', () => {
    it('creates a Message when a normal HL7 message is given', (done) => {
      const expectedMessage = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const message = new Message(expectedMessage);
      message.message.should.equal(expectedMessage);
      done();
    });

    it('rises an error when a Message is created when an abnormal HL7 message is given', (done) => {
      const msg = 'INVALID|HL7';
      (() => {
        // eslint-disable-next-line no-new
        new Message(msg);
      }).should.throw(TypeError, /Message constructor:: HL7 is not valid : /);
      done();
    });

    it('rises an error when a Message is created when no message is given', (done) => {
      (() => {
        // eslint-disable-next-line no-new
        new Message();
      }).should.throw(TypeError, 'Message constructor:: no message in parameter');
      done();
    });
  });

  describe('#getType', () => {
    it('gets the correct type of an HL7 ADT_A01 message', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expectedType = 'ADT^A01';

      const msg = new Message(message);
      msg.getType().should.equal(expectedType);
      done();
    });

    it('gets the correct type of an HL7 ADT_A05 message', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expectedType = 'ADT^A05';
      const parsedMsg = hl7.parse(message);
      parsedMsg.replace('MSH|9[1]', 'A05');

      const msg = new Message(parsedMsg.toString());
      msg.getType().should.equal(expectedType);
      done();
    });
  });

  describe('#getIpp', () => {
    it('gets the correct IPP list of an HL7 message where there is only one IPP', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expectedIpp = '68167^^^SIGEMS^PI';

      const msg = new Message(message);
      const ipp = msg.getIpp();
      ipp.should.be.an('array');
      ipp.should.have.length(1);
      ipp[0].should.equal(expectedIpp);
      done();
    });

    it('gets the correct IPP list of an HL7 message where there is multiple IPP', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expectedIpp = ['00402056^^^HM^PI', '50305437^^^SURGICA^PI', '1514715477417283614839^^^ASIP-SANTE&1.2.250.1.213.1.4.2&ISO^INS-C^^20160125'];

      const msg = new Message(message);
      const ipp = msg.getIpp();
      ipp.should.be.an('array');
      ipp.should.have.length(3);
      ipp[0].should.equal(expectedIpp[0]);
      ipp[1].should.equal(expectedIpp[1]);
      ipp[2].should.equal(expectedIpp[2]);
      done();
    });
  });

  describe('#getService', () => {
    it('returns the correct service of an HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expectedService = 'AMBU';

      const msg = new Message(message);
      const service = msg.getService();
      service.should.equal(expectedService);
      done();
    });
  });

  describe('#getEventDate', () => {
    it('returns the event date of the HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expectedDate = '20160205100000';

      const msg = new Message(message);
      const date = msg.getEventDate();
      date.should.equal(expectedDate);
      done();
    });
  });

  describe('#getBirthDate', () => {
    it('returns patient\'s birthdate', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expectedDate = '19521203';

      const msg = new Message(message);
      const date = msg.getBirthDate();
      date.should.equal(expectedDate);
      done();
    });

    it('returns an empty string instead of patient\'s birthdate when HL7 field is empty', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867_withoutBirthDate.HL7', 'utf8');
      const expectedDate = '';

      const msg = new Message(message);
      const date = msg.getBirthDate();
      date.should.equal(expectedDate);
      done();
    });
  });

  describe('#getRoom', () => {
    it('returns patient\'s assignated room', (done) => {
      const message = fs.readFileSync('./test/msgs/room.HL7', 'utf8');
      const expectedRoom = 'C315';

      const msg = new Message(message);
      const room = msg.getRoom();
      room.should.equal(expectedRoom);
      done();
    });

    it('returns an empty string instead of patient\'s room when HL7 field is empty', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00002855.HL7', 'utf8');
      const expectedRoom = '';

      const msg = new Message(message);
      const room = msg.getRoom();
      room.should.equal(expectedRoom);
      done();
    });
  });

  describe('#getLegalName', () => {
    it('returns the legal name if there is one in the HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expectedName = {
        name: 'LEPONGE',
        firstname: 'Bob'
      };

      const msg = new Message(message);
      const legalName = msg.getLegalName();
      legalName.should.be.an('object');
      legalName.should.have.property('name');
      legalName.should.have.property('firstname');
      legalName.name.should.equal(expectedName.name);
      legalName.firstname.should.equal(expectedName.firstname);
      done();
    });

    it('returns \'\' if there is no legal name in the HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/no_legalName.HL7', 'utf8');

      const msg = new Message(message);
      const legalName = msg.getLegalName();
      legalName.should.be.an('object');
      legalName.should.have.property('name');
      legalName.should.have.property('firstname');
      legalName.name.should.equal('');
      legalName.firstname.should.equal('');
      done();
    });
  });

  describe('#getDisplayName', () => {
    it('returns the display name if there is one in the HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expectedName = {
        name: 'CURIE',
        firstname: 'MARIE'
      };

      const msg = new Message(message);
      const displayName = msg.getDisplayName();
      displayName.should.be.an('object');
      displayName.should.have.property('name');
      displayName.should.have.property('firstname');
      displayName.name.should.equal(expectedName.name);
      displayName.firstname.should.equal(expectedName.firstname);
      done();
    });

    it('returns \'\' if there is no display name in the HL7 message', (done) => {
      const message = fs.readFileSync('./test/msgs/unknown.HL7', 'utf8');

      const msg = new Message(message);
      const displayName = msg.getDisplayName();
      displayName.should.be.an('object');
      displayName.should.have.property('name');
      displayName.should.have.property('firstname');
      displayName.name.should.equal('');
      displayName.firstname.should.equal('');
      done();
    });
  });

  describe('#_getAllNames', () => {
    it('gets all the names in the HL7 message when there is only one name', (done) => {
      const message = fs.readFileSync('./test/msgs/unknown.HL7', 'utf8');
      const expected = {
        L: ['PATIENT001', 'Patient001', 'MME', 'L']
      };

      const msg = new Message(message);
      const names = msg._getAllNames();
      names.should.be.an('object');
      names.should.have.property('L');
      names.L.should.be.an('array');
      names.L.should.have.length(4);
      names.L[0].should.equal(expected.L[0]);
      names.L[1].should.equal(expected.L[1]);
      names.L[2].should.equal(expected.L[2]);
      names.L[3].should.equal(expected.L[3]);
      done();
    });

    it('gets all the names in the HL7 message when there are two names', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expected = {
        L: [ 'SLADOWSKA', 'MARIA', 'L' ],
        D: [ 'CURIE', 'MARIE', 'D' ]
      };

      const msg = new Message(message);
      const names = msg._getAllNames();
      names.should.be.an('object');
      names.should.have.property('L');
      names.should.have.property('D');
      names.L.should.be.an('array');
      names.D.should.be.an('array');
      names.L.should.have.length(3);
      names.D.should.have.length(3);
      names.L[0].should.equal(expected.L[0]);
      names.L[1].should.equal(expected.L[1]);
      names.L[2].should.equal(expected.L[2]);
      names.D[0].should.equal(expected.D[0]);
      names.D[1].should.equal(expected.D[1]);
      names.D[2].should.equal(expected.D[2]);
      done();
    });
  });

  describe('#_getNameOfType', () => {
    it('gets the name of the corresponding type in the list of name', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expectedL = {
        name: 'SLADOWSKA',
        firstname: 'MARIA'
      };
      const expectedD = {
        name: 'CURIE',
        firstname: 'MARIE'
      };

      const msg = new Message(message);
      const Ltype = msg._getNameOfType('L');
      const Dtype = msg._getNameOfType('D');
      Ltype.should.be.an('object');
      Ltype.should.have.property('name');
      Ltype.should.have.property('firstname');
      Ltype.name.should.equal(expectedL.name);
      Ltype.firstname.should.equal(expectedL.firstname);
      Dtype.should.be.an('object');
      Dtype.should.have.property('name');
      Dtype.should.have.property('firstname');
      Dtype.name.should.equal(expectedD.name);
      Dtype.firstname.should.equal(expectedD.firstname);
      done();
    });

    it('returns \'\' if there is no name of the corresponding type in the list of name', (done) => {
      const message = fs.readFileSync('./test/msgs/unknown.HL7', 'utf8');

      const msg = new Message(message);
      const Dtype = msg._getNameOfType('D');
      Dtype.should.be.an('object');
      Dtype.should.not.have.property('name');
      Dtype.should.not.have.property('firstname');
      done();
    });
  });

  describe('#getPatientFromMessage', () => {
    it('extracts the good data from the message where all fields are filled up', (done) => {
      const message = fs.readFileSync('./test/msgs/HM00006867.HL7', 'utf8');
      const expected = {
        name: 'LEPONGE',
        firstname: 'Bob',
        maiden: '',
        birthDate: '03/12/1952',
        eventDate: '05/02/2016 10:00',
        ipp: ['00402056^^^HM^PI', '50305437^^^SURGICA^PI', '1514715477417283614839^^^ASIP-SANTE&1.2.250.1.213.1.4.2&ISO^INS-C^^20160125'],
        room: '199',
        service: 'AMBU',
        bed: '',
        sex: 'M'
      };

      const msg = new Message(message);
      const patient = msg.getPatientFromMessage();
      patient.should.be.an('object');
      patient.should.have.property('name');
      patient.should.have.property('firstname');
      patient.should.have.property('maiden');
      patient.should.have.property('birthDate');
      patient.should.have.property('eventDate');
      patient.should.have.property('ipp');
      patient.should.have.property('room');
      patient.should.have.property('service');
      patient.should.have.property('bed');
      patient.should.have.property('sex');
      patient.name.should.equal(expected.name);
      patient.firstname.should.equal(expected.firstname);
      patient.maiden.should.equal(expected.maiden);
      patient.birthDate.should.equal(expected.birthDate);
      patient.eventDate.should.equal(expected.eventDate);
      patient.ipp.should.be.an('array');
      patient.ipp.should.have.length(3);
      patient.ipp[0].should.equal(expected.ipp[0]);
      patient.ipp[1].should.equal(expected.ipp[1]);
      patient.ipp[2].should.equal(expected.ipp[2]);
      patient.room.should.equal(expected.room);
      patient.service.should.equal(expected.service);
      patient.bed.should.equal(expected.bed);
      patient.sex.should.equal(expected.sex);
      done();
    });

    it('extracts the data and puts some default values from the message where some fields are missing', (done) => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const expected = {
        name: 'CURIE',
        firstname: 'MARIE',
        maiden: 'SLADOWSKA',
        eventDate: '25/11/2015 12:30',
        ipp: ['68167^^^SIGEMS^PI']
      };

      const msg = new Message(message);
      const patient = msg.getPatientFromMessage();
      patient.should.be.an('object');
      patient.should.have.property('name');
      patient.should.have.property('firstname');
      patient.should.have.property('maiden');
      patient.should.have.property('eventDate');
      patient.should.have.property('ipp');
      patient.name.should.equal(expected.name);
      patient.firstname.should.equal(expected.firstname);
      patient.maiden.should.equal(expected.maiden);
      patient.eventDate.should.equal(expected.eventDate);
      patient.ipp.should.be.an('array');
      patient.ipp.should.have.length(1);
      patient.ipp[0].should.equal(expected.ipp[0]);
      done();
    });
  });
});
