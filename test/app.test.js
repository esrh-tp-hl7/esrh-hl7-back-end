/* global describe, it, before, after, beforeEach */
/**
 * @file app.test.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   Test file for the webservice api
 */
const chai = require('chai');
const should = chai.should();
const request = require('supertest');
const path = require('path');
const fs = require('fs');
const pkg = require('../package.json');
const config = require('../config');
const db = require('../lib/db');
const Patient = require('../models/patient');
const Message = require('../models/message');
const App = require('../lib/app');
const hl7Analyzer = require('../lib/hl7Analyzer');
const app = new App(config.port);

describe('Webservice api', () => {
  before(async () => {
    await db.connect(config.mongo);
    return app.start();
  });

  beforeEach(async () => {
    return db.clear();
  });

  it('gives a 404 response for an unknown request', (done) => {
    request(app.app)
      .get('/unknown')
      .expect(404, done);
  });

  describe('GET /version', () => {
    it('gives the version of the api when being requested on /version', (done) => {
      request(app.app)
        .get('/version')
        .expect(res => res.text.should.be.equals(pkg.version))
        .expect(200, done);
    });
  });

  describe('GET /patients', () => {
    it('returns an empty tab when there is no patient in the database', (done) => {
      request(app.app)
        .get('/patients')
        .expect(res => {
          try {
            res.text.should.be.a('String');
            const patients = JSON.parse(res.text);
            patients.should.be.an('Object');
            patients.should.have.property('patients');
            patients.patients.should.be.an('array').with.lengthOf(0);
          } catch (err) {
            should.fail();
          }
        })
        .expect(200, done);
    });

    it('returns 1 patient when there is one patient in the database', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      await new Patient(patient1).save();
      return request(app.app)
        .get('/patients')
        .expect(res => {
          try {
            res.text.should.be.a('String');
            const patients = JSON.parse(res.text);
            console.log('patients: ', patients);
            patients.should.be.an('Object');
            patients.should.have.property('patients');
            patients.patients.should.be.an('array').with.lengthOf(1);
            patients.patients[0].should.have.property('firstname', patient1.firstname);
            patients.patients[0].should.have.property('name', patient1.name);
            patients.patients[0].should.have.property('sex', patient1.sex);
            patients.patients[0].should.have.property('birthDate', patient1.birthDate);
            patients.patients[0].should.have.property('ipp');
            patients.patients[0].ipp.should.be.an('array').with.lengthOf(1);
            patients.patients[0].ipp[0].should.equals(patient1.ipp[0]);
          } catch (err) {
            should.fail();
          }
        })
        .expect(200);
    });

    it('returns 2 patients when there is 2 patients in the database', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      const patient2 = {
        firstname: 'Alpaga',
        name: 'Llama',
        sex: 'F',
        birthDate: '01/01/1990',
        ipp: ['000']
      };
      await new Patient(patient1).save();
      await new Patient(patient2).save();
      return request(app.app)
        .get('/patients')
        .expect(res => {
          try {
            res.text.should.be.a('String');
            const patients = JSON.parse(res.text);
            console.log('patients: ', patients);
            patients.should.be.an('Object');
            patients.should.have.property('patients');
            patients.patients.should.be.an('array').with.lengthOf(2);
            patients.patients[0].should.have.property('firstname', patient1.firstname);
            patients.patients[0].should.have.property('name', patient1.name);
            patients.patients[0].should.have.property('sex', patient1.sex);
            patients.patients[0].should.have.property('birthDate', patient1.birthDate);
            patients.patients[0].should.have.property('ipp');
            patients.patients[0].ipp.should.be.an('array').with.lengthOf(1);
            patients.patients[0].ipp[0].should.equals(patient1.ipp[0]);
            patients.patients[1].should.have.property('firstname', patient2.firstname);
            patients.patients[1].should.have.property('name', patient2.name);
            patients.patients[1].should.have.property('sex', patient2.sex);
            patients.patients[1].should.have.property('birthDate', patient2.birthDate);
            patients.patients[1].should.have.property('ipp');
            patients.patients[1].ipp.should.be.an('array').with.lengthOf(1);
            patients.patients[1].ipp[0].should.equals(patient2.ipp[0]);
          } catch (err) {
            console.log('err:', err);
            should.fail();
          }
        })
        .expect(200);
    });
  });

  describe('DELETE /reset', () => {
    it('doesn\'t clear the database when no password is given in parameter', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      const expected = {
        success: false,
        type: 'error',
        message: 'Try again (probably missing parameter)!'
      };
      await new Patient(patient1).save();
      return request(app.app)
        .delete('/reset')
        .expect(400)
        .expect(async (res) => {
          const response = res.body;
          response.should.have.property('success', expected.success);
          response.should.have.property('type', expected.type);
          response.should.have.property('message', expected.message);
          const patients = await Patient.find().lean();
          console.log('patients: ', patients);
          return patients.should.be.an('array').with.lengthOf(1);
        });
    });

    it('doesn\'t clear the database when given password is wrong', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      const expected = {
        success: false,
        type: 'error',
        message: 'Try again (probably missing parameter)!'
      };
      await new Patient(patient1).save();
      return request(app.app)
        .delete('/reset?pwd=alpaga')
        .expect(400)
        .expect(async (res) => {
          const response = res.body;
          response.should.have.property('success', expected.success);
          response.should.have.property('type', expected.type);
          response.should.have.property('message', expected.message);
          const patients = await Patient.find().lean();
          console.log('patients: ', patients);
          return patients.should.be.an('array').with.lengthOf(1);
        });
    });

    it('doesn\'t clear the database when a bad parameter is given', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      const expected = {
        success: false,
        type: 'error',
        message: 'Try again (probably missing parameter)!'
      };
      await new Patient(patient1).save();
      return request(app.app)
        .delete('/reset?alpaga=toto')
        .expect(400)
        .expect(async (res) => {
          const response = res.body;
          response.should.have.property('success', expected.success);
          response.should.have.property('type', expected.type);
          response.should.have.property('message', expected.message);
          const patients = await Patient.find().lean();
          console.log('patients: ', patients);
          return patients.should.be.an('array').with.lengthOf(1);
        });
    });

    it('clears the database when the correct password is given in paramater', async () => {
      const patient1 = {
        firstname: 'Toto',
        name: 'Tata',
        sex: 'M',
        birthDate: '01/01/1989',
        ipp: ['123']
      };
      const expected = {
        success: true,
        type: 'success',
        message: 'Database reset successful.'
      };
      await new Patient(patient1).save();
      return request(app.app)
        .delete('/reset?pwd=' + config.resetPasswd)
        .expect(200)
        .expect(async (res) => {
          const response = res.body;
          response.should.have.property('success', expected.success);
          response.should.have.property('type', expected.type);
          response.should.have.property('message', expected.message);
          const patients = await Patient.find().lean();
          console.log('patients: ', patients);
          return patients.should.be.an('array').with.lengthOf(0);
        });
    });
  });

  describe('POST /upload', () => {
    it('returns a 400 error when no file is uploaded', done => {
      request(app.app)
        .post('/upload')
        .expect(res => {
          const expected = {
            success: false,
            type: 'error',
            message: 'An error occured.'
          };
          try {
            const answer = JSON.parse(res.text);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        }, done)
        .expect(400, done);
    });

    it('returns a 400 error when the file uploaded is not an HL7 valid message', done => {
      request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, '../config.js'))
        .expect(res => {
          const expected = {
            success: false,
            type: 'error',
            message: 'Error while parsing message. Is it a valid HL7 message?'
          };
          try {
            const answer = JSON.parse(res.text);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        }, done)
        .expect(400, done);
    });

    it('returns a 200 when the file is an HL7 other than type A01, A02 and A03', done => {
      request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/HM00002855.HL7'))
        .expect(res => {
          const expected = {
            success: false,
            type: 'error',
            message: 'Message type is unknown or undefined'
          };
          try {
            const answer = JSON.parse(res.text);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        }, done)
        .expect(200, done);
    });

    it('returns an 200 and success for a new patient to admit with A01', done => {
      request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/1756116.HL7'))
        .expect(res => {
          const expected = {
            success: true,
            type: 'success',
            message: 'Patient has been admitted with success!'
          };
          try {
            const answer = JSON.parse(res.text);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        }, done)
        .expect(200, done);
    });

    it('returns an 200 and warning if the patient to admit is already known to the system with A01 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      await hl7Analyzer.analyze(parsedMsg);
      const patientCount = await Patient.find().countDocuments();
      patientCount.should.equal(1);

      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/1756116.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: false,
            type: 'warn',
            message: 'Patient already exists!'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and warning if the patient in A02 submitted is not admitted yet', async () => {
      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A02.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: false,
            type: 'warn',
            message: 'Patient has not been admitted yet.'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and warning if the patient in A02 is already discharged', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      await hl7Analyzer.analyze(parsedMsgA03);

      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A02.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: false,
            type: 'warn',
            message: 'Patient is already discharged!'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and success if the patient in A02 is transfered successfuly', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A02.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: true,
            type: 'success',
            message: 'Patient has been transfered with success.'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and warning if the patient in A03 has not been admitted yet', async () => {
      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A03.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: false,
            type: 'warn',
            message: 'Patient has not been admitted yet.'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and success if the patient in A03 has been discharged successfuly', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A03.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: true,
            type: 'success',
            message: 'Patient has been discharged with success.'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });

    it('returns an 200 and warning if the patient in A03 has already been discharged', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      await hl7Analyzer.analyze(parsedMsgA03);

      return request(app.app)
        .post('/upload')
        .attach('file', path.join(__dirname, './msgs/A03.HL7'))
        .expect(200)
        .expect(res => {
          const expected = {
            success: false,
            type: 'warn',
            message: 'Patient already discharged!'
          };
          try {
            const answer = JSON.parse(res.text);
            console.log('answer : ', answer);
            answer.should.be.an('Object');
            answer.should.have.property('success', expected.success);
            answer.should.have.property('type', expected.type);
            answer.should.have.property('message', expected.message);
          } catch (err) {
            console.log('error : ', err);
            should.fail();
          }
        });
    });
  });

  after(async () => {
    await db.clear();
    await db.close();
    await app.stop();
  });
});
