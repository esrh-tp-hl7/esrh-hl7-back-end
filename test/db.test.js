/* global describe, it, after */
/**
 * @file db.test.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   Test file for the db module
 */
const chai = require('chai');
// eslint-disable-next-line no-unused-vars
const should = chai.should();
const db = require('../lib/db');
const Patient = require('../models/patient');
const config = require('../config');

describe('Database module', () => {
  after(async () => {
    await db.close();
  });

  describe('#connect', () => {
    it('connects correctly to the database', async () => {
      return db.connect(config.mongo);
    });

    it('doesn\'t start a new connection if database is already connected', async () => {
      const url = config.mongo;
      await db.connect(url);
      const res = await db.connect(url);
      return res.should.equal('database already connected');
    });
  });

  describe('#close', () => {
    it('disconnects correctly from the database', async () => {
      return db.close();
    });
  });

  describe('#clear', () => {
    it('clears the database', async () => {
      await db.connect(config.mongo);
      let beforeCount = await Patient.find().countDocuments();
      await new Patient().save();
      let count = await Patient.find().countDocuments();
      count.should.equal(beforeCount + 1);
      await db.clear();
      count = await Patient.find().countDocuments();
      return count.should.equal(0);
    });
  });
});
