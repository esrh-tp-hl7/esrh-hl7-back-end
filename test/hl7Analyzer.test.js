/* global describe, it, before, after, afterEach */
/**
 * @file hl7Analyzer.test.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   Test file for the message model.
 */
const chai = require('chai');
// eslint-disable-next-line no-unused-vars
const should = chai.should();
const fs = require('fs');
const Message = require('../models/message');
const Patient = require('../models/patient');
const hl7Analyzer = require('../lib/hl7Analyzer');
const db = require('../lib/db');

describe('hl7Analyzer module', () => {
  before(async () => {
    return db.connect();
  });

  after(async () => {
    return db.close();
  });

  afterEach(async () => {
    return db.clear();
  });

  describe('#analyse', () => {
    it('returns an error object when there is no message and insert no patient in db', async () => {
      const expectedResult = {
        success: false,
        type: 'error',
        message: 'There must be a mistake somewhere...'
      };
      const res = await hl7Analyzer.analyze();
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      const patientCount = await Patient.find().countDocuments();
      return patientCount.should.equal(0);
    });

    it('returns an error object when the message type is different than A01, A02 and A03 and insert no patient in db', async () => {
      const message = fs.readFileSync('./test/msgs/HM00002855.HL7', 'utf8');
      const parsedMsg = new Message(message);
      const expectedResult = {
        success: false,
        type: 'error',
        message: 'Message type is unknown or undefined'
      };
      const res = await hl7Analyzer.analyze(parsedMsg);
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      const patientCount = await Patient.find().countDocuments();
      return patientCount.should.equal(0);
    });

    it('admits a fresh new patient with A01 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);
      const expectedResult = {
        success: true,
        type: 'success',
        message: 'Patient has been admitted with success!'
      };
      const expectedPatient = parsedMsg.getPatientFromMessage();
      expectedPatient.history = [{
        event: 'checkin',
        date: expectedPatient.eventDate,
        room: expectedPatient.room,
        service: expectedPatient.service,
        bed: expectedPatient.bed
      }];
      const res = await hl7Analyzer.analyze(parsedMsg);
      const patients = await Patient.find();
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      patients.should.be.an('Array').with.lengthOf(1);
      patients[0].should.be.an('Object');
      patients[0].should.have.property('firstname', expectedPatient.firstname);
      patients[0].should.have.property('ipp').with.lengthOf(1);
      patients[0].ipp[0].should.equal(expectedPatient.ipp[0]);
      patients[0].should.have.property('room', expectedPatient.room);
      patients[0].should.have.property('service', expectedPatient.service);
      patients[0].should.have.property('bed', expectedPatient.bed);
      patients[0].should.have.property('name', expectedPatient.name);
      patients[0].should.have.property('birthDate', expectedPatient.birthDate);
      patients[0].should.have.property('sex', expectedPatient.sex);
      patients[0].should.have.property('action', 'insert');
      patients[0].should.have.property('_id');
      patients[0].should.have.property('history');
      patients[0].history.should.be.an('array').with.lengthOf(1);
      patients[0].history[0].should.have.property('event', expectedPatient.history[0].event);
      patients[0].history[0].should.have.property('date', expectedPatient.history[0].date);
      patients[0].history[0].should.have.property('room', expectedPatient.history[0].room);
      patients[0].history[0].should.have.property('service', expectedPatient.history[0].service);
      patients[0].history[0].should.have.property('bed', expectedPatient.history[0].bed);
      return patients[0].should.have.property('surgeryDate', expectedPatient.eventDate);
    });

    it('returns a warning if the patient to admit is already known to the system with A01 message and insert no more patient', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      const firstAnalysis = await hl7Analyzer.analyze(parsedMsg);
      const expectedFirstAnalysis = {
        success: true,
        type: 'success',
        message: 'Patient has been admitted with success!'
      };
      firstAnalysis.should.be.an('Object');
      firstAnalysis.should.have.property('success', expectedFirstAnalysis.success);
      firstAnalysis.should.have.property('type', expectedFirstAnalysis.type);
      firstAnalysis.should.have.property('message', expectedFirstAnalysis.message);
      const patientCount = await Patient.find().countDocuments();
      patientCount.should.equal(1);

      const expectedResult = {
        success: false,
        type: 'warn',
        message: 'Patient already exists!'
      };
      const res = await hl7Analyzer.analyze(parsedMsg);
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      const patientCountAfter = await Patient.find().countDocuments();
      return patientCountAfter.should.equal(patientCount);
    });

    it('returns a warning if the patient to transfert is not admitted yet when receiving an A02 message and insert no patient', async () => {
      const message = fs.readFileSync('./test/msgs/A02.HL7', 'utf8');
      const parsedMsg = new Message(message);
      const expectedResult = {
        success: false,
        type: 'warn',
        message: 'Patient has not been admitted yet.'
      };
      const res = await hl7Analyzer.analyze(parsedMsg);
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      const patientCount = await Patient.find().countDocuments();
      return patientCount.should.equal(0);
    });

    it('returns a warning if the patient to transfert is already discharged yet when receiving an A02 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      await hl7Analyzer.analyze(parsedMsgA03);

      // Transfert
      const messageA02 = fs.readFileSync('./test/msgs/A02.HL7', 'utf8');
      const parsedA02Msg = new Message(messageA02);
      const expectedResult = {
        success: false,
        type: 'warn',
        message: 'Patient is already discharged!'
      };
      const res = await hl7Analyzer.analyze(parsedA02Msg);
      const patients = await Patient.find();
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      patients.should.be.an('Array').with.lengthOf(1);
      return patients[0].should.have.property('action', 'checkout');
    });

    it('returns a warning if the transfert date is before the last patient event date when receiving an A02 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      // Transfert
      const messageA02 = fs.readFileSync('./test/msgs/A02_old.HL7', 'utf8');
      const parsedA02Msg = new Message(messageA02);
      const expectedResult = {
        success: false,
        type: 'warn',
        message: 'Transfert date cannot be before admission/last transfert date'
      };
      const res = await hl7Analyzer.analyze(parsedA02Msg);
      const patients = await Patient.find();
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      patients.should.be.an('Array').with.lengthOf(1);
      return patients[0].should.have.property('action', 'insert');
    });

    it('transfers the patient when receiving an A02 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      const firstAnalysis = await hl7Analyzer.analyze(parsedMsg);
      const expectedFirstAnalysis = {
        success: true,
        type: 'success',
        message: 'Patient has been admitted with success!'
      };
      firstAnalysis.should.be.an('Object');
      firstAnalysis.should.have.property('success', expectedFirstAnalysis.success);
      firstAnalysis.should.have.property('type', expectedFirstAnalysis.type);
      firstAnalysis.should.have.property('message', expectedFirstAnalysis.message);
      const patientCount = await Patient.find().countDocuments();
      patientCount.should.equal(1);

      const messageA02 = fs.readFileSync('./test/msgs/A02.HL7', 'utf8');
      const parsedA02Msg = new Message(messageA02);
      const expectedResult = {
        success: true,
        type: 'success',
        message: 'Patient has been transfered with success.'
      };
      const expectedPatient = parsedA02Msg.getPatientFromMessage();
      expectedPatient.action = 'transfert';
      expectedPatient.history = [{
        event: 'checkin'
      },
      {
        event: 'transfert',
        date: expectedPatient.eventDate,
        room: expectedPatient.room,
        service: expectedPatient.service,
        bed: expectedPatient.bed
      }
      ];

      const res = await hl7Analyzer.analyze(parsedA02Msg);
      const patients = await Patient.find();
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      patients[0].should.be.an('Object');
      patients[0].should.have.property('firstname', expectedPatient.firstname);
      patients[0].should.have.property('ipp').with.lengthOf(1);
      patients[0].ipp[0].should.equal(expectedPatient.ipp[0]);
      patients[0].should.have.property('room', expectedPatient.room);
      patients[0].should.have.property('service', expectedPatient.service);
      patients[0].should.have.property('bed', expectedPatient.bed);
      patients[0].should.have.property('name', expectedPatient.name);
      patients[0].should.have.property('birthDate', expectedPatient.birthDate);
      patients[0].should.have.property('sex', expectedPatient.sex);
      patients[0].should.have.property('action', expectedPatient.action);
      patients[0].should.have.property('history');
      patients[0].history.should.be.an('array').with.lengthOf(2);
      patients[0].history[0].should.have.property('event', expectedPatient.history[0].event);
      patients[0].history[1].should.have.property('event', expectedPatient.history[1].event);
      patients[0].history[1].should.have.property('date', expectedPatient.history[1].date);
      patients[0].history[1].should.have.property('room', expectedPatient.history[1].room);
      patients[0].history[1].should.have.property('service', expectedPatient.history[1].service);
      patients[0].history[1].should.have.property('bed', expectedPatient.history[1].bed);
      return patients[0].should.have.property('_id');
    });

    it('returns a warning if the patient to discharge has not been admitted yet when receiving an A03 message', async () => {
      const message = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsg = new Message(message);
      const expectedResult = {
        success: false,
        type: 'warn',
        message: 'Patient has not been admitted yet.'
      };
      const res = await hl7Analyzer.analyze(parsedMsg);
      res.should.be.an('Object');
      res.should.have.property('success', expectedResult.success);
      res.should.have.property('type', expectedResult.type);
      res.should.have.property('message', expectedResult.message);
      const patientCount = await Patient.find().countDocuments();
      return patientCount.should.equal(0);
    });

    it('discharged the patient when receiving an A03 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      const firstAnalysis = await hl7Analyzer.analyze(parsedMsg);
      const expectedFirstAnalysis = {
        success: true,
        type: 'success',
        message: 'Patient has been admitted with success!'
      };
      firstAnalysis.should.be.an('Object');
      firstAnalysis.should.have.property('success', expectedFirstAnalysis.success);
      firstAnalysis.should.have.property('type', expectedFirstAnalysis.type);
      firstAnalysis.should.have.property('message', expectedFirstAnalysis.message);
      const patientCount = await Patient.find().countDocuments();
      patientCount.should.equal(1);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      const expectedPatientA03 = parsedMsgA03.getPatientFromMessage();
      expectedPatientA03.action = 'checkout';
      expectedPatientA03.checkoutDate = expectedPatientA03.eventDate;
      expectedPatientA03.history = [{
        event: 'checkin'
      },
      {
        event: 'checkout',
        date: expectedPatientA03.eventDate
      }
      ];
      const resA03 = await hl7Analyzer.analyze(parsedMsgA03);
      const patients = await Patient.find();
      const expectedA03 = {
        success: true,
        type: 'success',
        message: 'Patient has been discharged with success.'
      };
      resA03.should.be.an('Object');
      resA03.should.have.property('success', expectedA03.success);
      resA03.should.have.property('type', expectedA03.type);
      resA03.should.have.property('message', expectedA03.message);
      patients.should.be.an('Array').with.lengthOf(1);
      patients[0].should.be.an('Object');
      patients[0].should.have.property('firstname', expectedPatientA03.firstname);
      patients[0].should.have.property('ipp').with.lengthOf(1);
      patients[0].ipp[0].should.equal(expectedPatientA03.ipp[0]);
      patients[0].should.have.property('room', expectedPatientA03.room);
      patients[0].should.have.property('service', expectedPatientA03.service);
      patients[0].should.have.property('bed', expectedPatientA03.bed);
      patients[0].should.have.property('name', expectedPatientA03.name);
      patients[0].should.have.property('birthDate', expectedPatientA03.birthDate);
      patients[0].should.have.property('sex', expectedPatientA03.sex);
      patients[0].should.have.property('action', expectedPatientA03.action);
      patients[0].should.have.property('checkoutDate', expectedPatientA03.checkoutDate);
      patients[0].should.have.property('history');
      patients[0].history.should.be.an('array').with.lengthOf(2);
      patients[0].history[0].should.have.property('event', expectedPatientA03.history[0].event);
      patients[0].history[1].should.have.property('event', expectedPatientA03.history[1].event);
      patients[0].history[1].should.have.property('date', expectedPatientA03.history[1].date);
      return patients[0].should.have.property('_id');
    });

    it('returns a warning if the discharged date is before the previous action dates when receiving an A03 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      const firstAnalysis = await hl7Analyzer.analyze(parsedMsg);
      const expectedFirstAnalysis = {
        success: true,
        type: 'success',
        message: 'Patient has been admitted with success!'
      };
      firstAnalysis.should.be.an('Object');
      firstAnalysis.should.have.property('success', expectedFirstAnalysis.success);
      firstAnalysis.should.have.property('type', expectedFirstAnalysis.type);
      firstAnalysis.should.have.property('message', expectedFirstAnalysis.message);
      const patientCount = await Patient.find().countDocuments();
      patientCount.should.equal(1);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03_old.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      const resA03 = await hl7Analyzer.analyze(parsedMsgA03);
      const expectedA03 = {
        success: false,
        type: 'warn',
        message: 'Discharge date cannot be before admission/transfert date'
      };
      resA03.should.be.an('Object');
      resA03.should.have.property('success', expectedA03.success);
      resA03.should.have.property('type', expectedA03.type);
      return resA03.should.have.property('message', expectedA03.message);
    });

    it('returns a warning if the patient to discharge has already been discharged when receiving an A03 message', async () => {
      const message = fs.readFileSync('./test/msgs/1756116.HL7', 'utf8');
      const parsedMsg = new Message(message);

      // First admission
      await hl7Analyzer.analyze(parsedMsg);

      // Discharge
      const messageA03 = fs.readFileSync('./test/msgs/A03.HL7', 'utf8');
      const parsedMsgA03 = new Message(messageA03);
      await hl7Analyzer.analyze(parsedMsgA03);

      // Second discharge
      const resA03 = await hl7Analyzer.analyze(parsedMsgA03);
      const expectedA03 = {
        success: false,
        type: 'warn',
        message: 'Patient already discharged!'
      };
      resA03.should.be.an('Object');
      resA03.should.have.property('success', expectedA03.success);
      resA03.should.have.property('type', expectedA03.type);
      resA03.should.have.property('message', expectedA03.message);
      const patientCount = await Patient.find().countDocuments();
      return patientCount.should.equal(1);
    });
  });
});
