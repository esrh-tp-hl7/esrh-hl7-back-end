module.exports = {
  logLevel: process.env.LOGLEVEL || 'debug',
  port: process.env.PORT || 8080,
  mongo: process.env.MONGO || 'mongodb://mongo:27017/esrh-patients',
  hl7EncodingFormat: process.env.HL7_ENCODING || 'utf8',
  resetPasswd: process.env.RESET_PASSWD || 'yolo'
};
