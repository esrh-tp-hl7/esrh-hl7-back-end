FROM node:10-alpine
ARG VERSION
ARG PRODUCT

LABEL \
  version=${VERSION} \
  product=${PRODUCT}

COPY . /app
RUN chown -R node /app
USER node
WORKDIR /app
RUN npm install --production

ENTRYPOINT [ "npm" ]
CMD ["start"]

HEALTHCHECK --interval=5m --timeout=10s \
    CMD node healthCheck.js || exit 1

EXPOSE 8080
