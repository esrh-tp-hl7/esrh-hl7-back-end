/**
 * @file hl7Analyzer.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   This module contains the HL7 analysis part. It will analyse the received HL7 messages
 *   and update the app database accordingly.
 */
const bunyan = require('bunyan');
const moment = require('moment');
const Patient = require('../models/patient');
const config = require('../config');
const logger = bunyan.createLogger({
  name: 'hl7Analyzer',
  level: config.logLevel
});

module.exports = {

  async analyze (message) {
    logger.info('Starting HL7 analysis...');
    let analysisReturn = {
      success: false,
      type: 'error',
      message: 'There must be a mistake somewhere...'
    };

    if (!message) {
      logger.error('No message : AnalysisReturn : ', analysisReturn);
      return analysisReturn;
    }

    const patientInfo = message.getPatientFromMessage();
    logger.debug('patientInfo : ', patientInfo);
    const type = message.getType();
    logger.debug('type:', type);

    switch (type) {
      case 'ADT^A01':
        analysisReturn = await _A01(patientInfo);
        break;
      case 'ADT^A02':
        analysisReturn = await _A02(patientInfo);
        break;
      case 'ADT^A03':
        analysisReturn = await _A03(patientInfo);
        break;
      default:
        const errorMsg = 'Message type is unknown or undefined';
        logger.info(errorMsg);
        analysisReturn.message = errorMsg;
        break;
    }
    return analysisReturn;
  }
};

const _A01 = async (patientInfo) => {
  logger.debug('A01');
  try {
    const patient = await Patient.findOne({ ipp: patientInfo.ipp });
    if (patient) return _createResult(false, 'Patient already exists!');
    patientInfo.action = 'insert';
    patientInfo.surgeryDate = patientInfo.eventDate;
    patientInfo.history = [{
      event: 'checkin',
      date: patientInfo.eventDate,
      room: patientInfo.room,
      service: patientInfo.service,
      bed: patientInfo.bed
    }];
    const newPatient = new Patient(patientInfo);
    await newPatient.save();
    return _createResult(true, 'Patient has been admitted with success!');
  } catch (err) {
    logger.error('Error while finding patient for A01: ', err);
  }
};

const _A02 = async (patientInfo) => {
  logger.debug('A02');
  try {
    const patient = await Patient.findOne({ ipp: patientInfo.ipp });
    if (!patient) return _createResult(false, 'Patient has not been admitted yet.');
    if (patient.checkoutDate) return _createResult(false, 'Patient is already discharged!');
    if (_isPrevious(patientInfo.eventDate, patient.surgeryDate)) return _createResult(false, 'Transfert date cannot be before admission/last transfert date');
    patientInfo.action = 'transfert';
    patient.history.push({
      event: 'transfert',
      date: patientInfo.eventDate,
      room: patientInfo.room,
      service: patientInfo.service,
      bed: patientInfo.bed
    });
    await patient.save();
    await patient.update(patientInfo);
    return _createResult(true, 'Patient has been transfered with success.');
  } catch (err) {
    logger.error('Error while finding patient for A02: ', err);
  }
};

const _A03 = async (patientInfo) => {
  logger.debug('A03');
  try {
    const patient = await Patient.findOne({ ipp: patientInfo.ipp });
    if (!patient) return _createResult(false, 'Patient has not been admitted yet.');
    if (patient.checkoutDate) return _createResult(false, 'Patient already discharged!');
    if (_isPrevious(patientInfo.eventDate, patient.surgeryDate)) return _createResult(false, 'Discharge date cannot be before admission/transfert date');
    patientInfo.action = 'checkout';
    patientInfo.checkoutDate = patientInfo.eventDate;
    patient.history.push({
      event: 'checkout',
      date: patientInfo.eventDate
    });
    await patient.save();
    await patient.update(patientInfo);
    return _createResult(true, 'Patient has been discharged with success.');
  } catch (err) {
    logger.error('Error while finding patient for A03: ', err);
  }
};

const _createResult = (success, message) => {
  return {
    success: success,
    type: success ? 'success' : 'warn',
    message: message
  };
};

const _isPrevious = (newDate, refDate) => {
  return moment(newDate, 'DD/MM/YYYY HH:mm').isBefore(moment(refDate, 'DD/MM/YYYY HH:mm'));
};
