/**
 * @file db.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   This module is the interface with the Mongo database: it can connect/disconnect to the database
 *   and also clear the database.
 */
const mongoose = require('mongoose');
const bunyan = require('bunyan');
const config = require('../config');
const logger = bunyan.createLogger({
  name: 'db',
  level: config.logLevel
});

module.exports = {

  async connect (mongoUrl) {
    logger.debug('Connection to database...');
    const url = mongoUrl || config.mongo;

    if (mongoose.connection.readyState === 1) {
      const errorMsg = 'database already connected';
      logger.info(errorMsg);
      return errorMsg;
    }
    await mongoose.connect(url, { useNewUrlParser: true });
    const db = mongoose.connection;
    db.on('error', err => { return logger.error('Database error: ', err); });
    logger.info('Database connected at ', url);
  },

  async close () {
    logger.debug('Closing database connection...');
    await mongoose.connection.close();
    logger.info('database closed');
  },

  async clear () {
    logger.debug('Clearing database content...');
    await mongoose.connection.dropDatabase();
    logger.info('Database cleared');
  }
};
