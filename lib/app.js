/**
 * @file app.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   This module is handling the web server of the application itself. It can start it, stop it
 *   and manage the different routes.
 */
const bunyan = require('bunyan');
const express = require('express');
const cors = require('cors');
const iconv = require('iconv-lite');
const multer = require('multer');
const upload = multer();
const socket = require('socket.io');
const pkg = require('../package.json');
const Patient = require('../models/patient');
const Message = require('../models/message');
const hl7Analyzer = require('./hl7Analyzer');
const config = require('../config');
const logger = bunyan.createLogger({
  name: 'App',
  level: config.logLevel
});

class App {
  constructor (port) {
    logger.debug('Server constructor called with port: ', port);
    this.port = port || config.port;
    this.app = express();
    this.server = null;
    this.io = null;

    this.app.use(cors());
    this.app.use((req, res, next) => {
      logger.info('Socket magic: ', req.method, req.originalUrl);
      req.io = this.io;
      next();
    });
    this.app.get('/version', this._getVersion);
    this.app.get('/patients', this._getPatients);
    this.app.post('/upload', upload.single('file'), this._uploadFile);
    this.app.delete('/reset', this._resetDb);
    this.app.use((req, res) => {
      logger.info('No route: ', req.method, req.originalUrl);
      logger.warn('404', req.method, req.originalUrl);
      res.sendStatus(404);
    });
  }

  async start () {
    this.server = await this.app.listen(this.port);
    this.io = await socket(this.server);
    this.io.on('connection', socket => {
      logger.info('Socket connected: ', socket.id);
    });
    return logger.info('Server started on port: ', this.port);
  }

  async stop () {
    await this.server.close();
    logger.info('Server stopped');
  }

  _getVersion (req, res) {
    logger.info('/version', req.method, req.originalUrl);
    res.send(pkg.version);
  }

  async _getPatients (req, res) {
    logger.info('/patients', req.method, req.originalUrl);
    try {
      const patients = await Patient.find({}, 'firstname sex name service room bed surgeryDate birthDate ipp checkoutDate action history').lean();
      logger.debug('Patients found: ', patients);
      res.send({
        patients: patients
      });
    } catch (err) {
      logger.error('Error occured while finding patients: ', err);
      res.status(500).send('Error occured while finding patients');
    }
  }

  async _uploadFile (req, res) {
    logger.info('/upload', req.method, req.originalUrl);
    let result = {
      success: false,
      type: 'error',
      message: 'An error occured.'
    };
    let code = 400;

    if (req.file) {
      const msg = iconv.decode(req.file.buffer, config.hl7EncodingFormat);
      try {
        const message = new Message(msg);
        result = await hl7Analyzer.analyze(message);
        req.io.emit('refresh', {});
        code = 200;
      } catch (err) {
        logger.error('Error while parsing message: ', err);
        result.message = 'Error while parsing message. Is it a valid HL7 message?';
      }
    }
    return res.status(code).send(result);
  }

  async _resetDb (req, res) {
    logger.info('/reset', req.method, req.originalUrl);
    let result = {
      success: false,
      type: 'error',
      message: 'Try again (probably missing parameter)!'
    };
    let code = 400;
    if (req.query.pwd && req.query.pwd === config.resetPasswd) {
      logger.debug('password: ', req.query.pwd);
      try {
        await Patient.deleteMany();
        req.io.emit('refresh', {});
        result.success = true;
        result.type = 'success';
        result.message = 'Database reset successful.';
        code = 200;
      } catch (err) {
        logger.error('Error while reseting database: ', err);
      }
    }
    return res.status(code).send(result);
  }
}

module.exports = App;
