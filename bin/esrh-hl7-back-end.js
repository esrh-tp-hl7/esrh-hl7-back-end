/**
 * @file esrh-hl7-back-end.js
 * @author Clotaire Delanchy <clo.delanchy@gmail.com>
 * @description
 *   Script starting the back-end for the ESRH HL7 webservice, connecting the database and starting the HL7 module.
 */
const bunyan = require('bunyan');
const App = require('../lib/app');
const config = require('../config');
const db = require('../lib/db');
const logger = bunyan.createLogger({
  name: 'esrh-hl7-back-end',
  level: config.logLevel
});

logger.info('*************************************');
logger.info('********** ESRH HL7 TP **************');
logger.info('************ BACK-END ***************');
logger.info('*************************************');

async function main () {
  try {
    await db.connect(config.mongo);
    await new App().start();
  } catch (err) {
    logger.error('Error : ', err);
  }
}

main();
