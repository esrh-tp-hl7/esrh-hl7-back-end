const bunyan = require('bunyan');
const hl7 = require('L7');
const moment = require('moment');
const config = require('../config');
const logger = bunyan.createLogger({
  name: 'Message',
  level: config.logLevel
});

class Message {
  constructor (message) {
    logger.debug('creating new Message');

    if (!message) throw new TypeError('Message constructor:: no message in parameter');

    // Check params and directories
    this.message = message;
    this.parsedMsg = hl7.parse(this.message);
    if (typeof this.parsedMsg.error !== 'function') {
      logger.error('Message constructor:: HL7 is not valid : ' + this.parsedMsg.error);
      throw new TypeError('Message constructor:: HL7 is not valid : ', this.parsedMsg.error);
    }
  }

  getType () {
    let type = this.parsedMsg.query('MSH|9[0]') + '^' + this.parsedMsg.query('MSH|9[1]');
    logger.debug('getType: ', type);
    return type;
  }

  getIpp () {
    let ipp = this.parsedMsg.query('PID|3').split('~');
    logger.debug('getIpp: ', ipp);
    return ipp;
  }

  getLegalName () {
    let res = this._getNameOfType('L');
    if (!res.name) res.name = '';
    if (!res.firstname) res.firstname = '';
    logger.debug('getLegalName: ', res);
    return res;
  }

  getDisplayName () {
    let res = this._getNameOfType('D');
    if (!res.name) res.name = '';
    if (!res.firstname) res.firstname = '';
    logger.debug('getDisplayName: ', res);
    return res;
  }

  /**
   * Returns a String containing the birthdate:
   *'19521203'
   * @returns {''}
   */
  getBirthDate () {
    const birthDate = this.parsedMsg.query('PID|7');
    logger.debug('getBirthDate: ', birthDate);
    return birthDate;
  }

  /**
   * Returns a String containing the room number:
   *'P315'
   * @returns {''}
   */
  getRoom () {
    const room = this.parsedMsg.query('PV1|3[1]')[0];
    logger.debug('getRoom: ', room);
    return room;
  }

  getService () {
    const service = this.parsedMsg.query('PV1|3[0]')[0];
    logger.debug('getService: ', service);
    return service;
  }

  getBed () {
    return this.parsedMsg.query('PV1|3[2]')[0];
  }

  getSex () {
    return this.parsedMsg.query('PID|8')[0];
  }

  getEventDate () {
    const eventDate = this.parsedMsg.query('EVN|6');
    logger.debug('getEventDate: ', eventDate);
    return eventDate;
  }

  /**
   * Returns an object containing tabs:
   * {
   *      L: [ 'CAMBOURD', 'MONIQUE', 'L' ],
   *      D: [ 'VERHULST', 'MONIQUE', 'D' ]
   * }
   * @returns {{}}
   * @private
   */
  _getAllNames () {
    let names = this.parsedMsg.query('PID|5').split('~');
    let namesResults = {};

    // Extract all names
    let nameTab;
    let type;
    let content;
    names.forEach(name => {
      nameTab = this._parseName(name);
      content = name.split('^');
      type = content[6];
      namesResults[type] = nameTab;
    });
    logger.debug('_getAllNames : ', namesResults);
    return namesResults;
  }

  /**
   * Returns an object with property name & firstname or an empty one
   * @param nameType
   * @returns {{}}
   * @private
   */
  _getNameOfType (nameType) {
    let names = this._getAllNames();
    let res = {};
    if (names.hasOwnProperty(nameType)) {
      res = {
        name: names[nameType][0],
        firstname: names[nameType][1]
      };
    }
    logger.debug('_getNameOfType: ', res);
    return res;
  }

  _parseName (name) {
    let regex = new RegExp('\\^+', 'g');
    let output = name.replace(regex, ',').split(',');
    logger.debug('_parseName: ', output);
    return output;
  }

  getPatientFromMessage () {
    let displayName = this.getDisplayName();
    let legalName = this.getLegalName();
    let name = displayName.name === '' ? legalName.name : displayName.name;
    let maiden = displayName.name !== '' ? legalName.name : '';
    let firstname = displayName.firstname === '' ? legalName.firstname : displayName.firstname;
    let patient = {
      name: name,
      firstname: firstname,
      maiden: maiden,
      birthDate: moment(this.getBirthDate()).format('DD/MM/YYYY'),
      eventDate: moment(this.getEventDate(), 'YYYYMMDDHHmmss').format('DD/MM/YYYY HH:mm'),
      ipp: this.getIpp(),
      room: this.getRoom(),
      service: this.getService(),
      bed: this.getBed(),
      sex: this.getSex()
    };
    logger.debug('getPatientFromMessage : ', patient);
    return patient;
  }
}

module.exports = Message;
