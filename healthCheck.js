'use strict';

const { get } = require('http');
const config = require('./config');

get('http://localhost:' + config.port + '/version', res => {
  const { statusCode } = res;
  if (statusCode !== 200) {
    console.log(`ht: bad status code ${statusCode}`);
    process.exit(1);
  }
}).on('error', err => {
  console.error('ht: unexpected error', err);
  process.exit(1);
});
