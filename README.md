# ESRH HL7 Back-end [![pipeline status](https://gitlab.com/esrh-tp-hl7/esrh-hl7-back-end/badges/develop/pipeline.svg)](https://gitlab.com/esrh-tp-hl7/esrh-hl7-back-end/commits/develop) [![coverage report](https://gitlab.com/esrh-tp-hl7/esrh-hl7-back-end/badges/develop/coverage.svg)](https://gitlab.com/esrh-tp-hl7/esrh-hl7-back-end/commits/develop) [![licence](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This project is the back-end for the ESRH HL7 TP project. It is a webservice that has an API accessible through HTTP requests.
You can build it from source, but also directly use the builded Docker image.

## Start the service

You can start the service from the source or using the Docker image provided.

Using sources:

    npm i
    npm start

Using Docker:

    docker pull esrh-back-end:latest
    docker run --link alpaga-mongo:mongo --name esrh-back-end -p 8080:8080 esrh-hl7-back-end-test:latest

## Configuration

A configuration file is provided for this application. You can find it directly at the root of the project, a file named `config.js`.

It has the following keys:

* **`logLevel`**: set the log level output for the entire application. It uses `bunyan`, so authorized values are `debug|info|warn|error`. Default to 'debug'.
* **`port`**: port on which the webservice will listen. Default to 8080.
* **`mongo`**: address on which mongodb is accessible. Default to 'mongodb://mongo:27017/esrh-patients'.
* **`hl7EncodingFormat`**: encoding of the file format to read. Default to 'utf8'. Authorized values are visible on the `iconv-lite` lib website.
* **`resetPasswd`**: password needed to clear the database. Default to 'yolo'.

All this parameters are accessible as environment variables:

| Variable   | Parameters              | Defaults                                |
|------------|-------------------------|-----------------------------------------|
|LOGLEVEL    | Set the bunyan logLevel |'debug'                                  |
|PORT        | Port to listen to       |8080                                     |
|MONGO       | Mongo db address        |'mongodb://localhost:27017/esrh-patients'|
|HL7_ENCODING|File encoding            |'utf8'                                   |
|RESET_PASSWD|Reset password           |'yolo'                                   |

## API

| Route   | Verb | Parameters | Output                                                                                |
|-------  |------|------------|---------------------------------------------------------------------------------------|
|/version |GET   |N/A         |Returns the version of the application from the package.json file                      |
|/patients|GET   |N/A         |Returns the patients in the application in a tab                                       |
|/upload  |POST  |file        |Returns the HL7 analysis from the attached file                                        |
|/reset   |DELETE|pwd         |Clears the database if the given password in query is the one in the configuration file|

## Build

### From source

To build the app from source, you need nodejs version 10 or superior. Then, proceed as followed after cloning the project.

    npm i

### Docker image

You can build the associated Docker image doing the following:

    docker build .

## Tests

Unit tests are provided with this webservice and are accessible in the `./test` folder. You will find the test files and also some HL7 sample files provided for testing purpose.

> Before starting the test suite, ensure you have a mongodb accessible and to have completed correctly the `./config.js` `mongo` property accordingly.

You can run the test suites as follows:

    npm i
    npm test

To get a more detailed coverage output, run:

    npm run coverage

## Contributing

Contributions are welcome! If you'd like to help, please, ensure that you follows the [contribution guidelines](./CONTRIBUTING.md).
